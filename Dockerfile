### Compléter avec les mots clés (instructions) manquants. ### 

# Utiliser une image Python officielle comme base (ex: python:3.8)

# Créer un répertoire de travail app dans le conteneur

# Copier le fichier app.py de votre machine dans le répertoire app du conteneur (syntaxe: KEYWORD $fichier_source $destination)

# Installer Flask dans le conteneur (syntaxe: KEYWORD pip install --no-cache-dir Flask)

# Exposer le port 5000 sur lequel l'application va écouter

# Commande pour démarrer l'application (syntaxe: KEYWORD ["$arg1, "$arg2"])
